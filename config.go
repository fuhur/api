package main

// Config is a struct to define configuration
type Config struct {
	Port          int    `default:"10292"`
	Token         string `required:"true"`
	SignSecret    string `required:"true"`
	RedisAddr     string `default:"localhost:6379"`
	RedisPassword string
	RedisDB       int `default:"1"`
}
