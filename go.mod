module gitlab.com/fuhur/api

replace github.com/Sirupsen/logrus => github.com/sirupsen/logrus v1.4.1

go 1.12

require (
	github.com/Sirupsen/logrus v1.4.1
	github.com/coreos/go-systemd v0.0.0-20190321100706-95778dfbb74e
	github.com/go-redis/redis v6.15.2+incompatible
	github.com/google/uuid v1.3.0 // indirect
	github.com/kelseyhightower/envconfig v1.3.0
	github.com/onsi/ginkgo v1.8.0 // indirect
	github.com/onsi/gomega v1.5.0 // indirect
	github.com/sirupsen/logrus v1.4.2 // indirect
	gitlab.com/fuhur/execjob v1.0.1
	gitlab.com/fuhur/redis-rangereader v1.0.1
)
