package main

import (
	"errors"
	"flag"
	"fmt"
	"net"
	"net/http"
	"os"
	"os/signal"

	log "github.com/Sirupsen/logrus"
	"github.com/coreos/go-systemd/activation"
	"github.com/coreos/go-systemd/daemon"
	"github.com/go-redis/redis"
	"github.com/kelseyhightower/envconfig"
)

var config Config
var redisClient *redis.Client
var version string

var versionFlag bool

const hookTokenHeaderName string = "X-HOOK-TOKEN"
const jobChannelName = "job"

// ErrNoExecConf an error which used when no execution configuration foiund for a key
var ErrNoExecConf = errors.New("No execConf found")

func hasValidToken(r *http.Request) bool {
	return r.Header.Get(hookTokenHeaderName) == config.Token
}

func main() {
	flag.BoolVar(&versionFlag, "version", false, "show version number")
	flag.Parse()
	if versionFlag {
		fmt.Printf("%s\n", version)
		os.Exit(0)
	}

	if err := envconfig.Process("FUHUR", &config); err != nil {
		log.Fatal(err)
	}

	redisClient = redis.NewClient(&redis.Options{
		Addr:     config.RedisAddr,
		Password: config.RedisPassword,
		DB:       config.RedisDB,
	})

	listeners, err := activation.Listeners()
	if err != nil {
		log.Panic(err)
	}

	var l net.Listener

	if len(listeners) == 0 {
		l, err = net.Listen("tcp", fmt.Sprintf(":%d", config.Port))
		if err != nil {
			log.Panic(err)
		}
	} else if len(listeners) != 1 {
		panic("Unexpected number of socket activation fds")
	} else {
		l = listeners[0]
	}

	http.HandleFunc("/", RequestHandler)

	done := make(chan error)
	osSignals := make(chan os.Signal, 1)
	signal.Notify(osSignals, os.Interrupt)

	go func() {
		done <- http.Serve(l, nil)
	}()

	daemon.SdNotify(false, "READY=1")
	log.Info(fmt.Sprintf("Listening on port %s", l.Addr()))

	select {
	case err := <-done:
		if err != nil {
			log.Fatal(err)
		}
	case <-osSignals:
		log.Info("Stop server")
		daemon.SdNotify(false, "STOPPING=1")
		l.Close()
	}
}
