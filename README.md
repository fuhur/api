# Fuhur

Fuhur meant to be a small deployment server for _one machine environment_ with the capability of extending it to multiple machines.

The base idea is that the **programs** you want **to run on your server** (for example a static website or a simple nodejs application) **are built by an external service** (like gitlab ci/cd) but you still need to **deliver** those **programs to your own server**.

That's where _Fuhur_ can help. It has two parts, one is the **API server**, the other is the **agent**.

The webserver must be called when you want to deliver some software. That call will create a job what will be processed by the agent.

# API Server

This package is the **API Server** part of _Fuhur_.
