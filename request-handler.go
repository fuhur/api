package main

import (
	"encoding/json"
	"fmt"
	"io"
	"math/rand"
	"net/http"
	"strings"

	log "github.com/Sirupsen/logrus"
	"github.com/google/uuid"
	"gitlab.com/fuhur/execjob"
	redisrangereader "gitlab.com/fuhur/redis-rangereader"
)

const redisKeyPrefix string = "redis_logs:"
const hookJobHeaderName string = "X-HOOK-JOB"

func randInt(min int, max int) int {
	return min + rand.Intn(max-min)
}

func generateJobID() string {
	id := uuid.New()
	return id.String()
}

func handleNewJobRequest(w http.ResponseWriter, r *http.Request) {
	jobName := r.Header.Get(hookJobHeaderName)
	job := r.Header.Get(hookJobHeaderName)

	if job == "" {
		http.Error(w, "Bad request", http.StatusBadRequest)
		return
	}

	jsonBody, err := getJSONBody(r)

	if err != nil {
		http.Error(w, "Invalid request", http.StatusNotAcceptable)
		return
	}

	jobID := generateJobID()

	signedJob, err := execjob.SignJob(config.SignSecret, execjob.Job{
		JobName: jobName,
		JobID:   jobID,
		Env:     jsonBody.Env,
	})

	if err != nil {
		http.Error(w, "InternalServerError", http.StatusInternalServerError)
		return
	}

	msg, err := json.Marshal(signedJob)

	if err != nil {
		http.Error(w, "InternalServerError", http.StatusInternalServerError)
		return
	}
	res := redisClient.Publish(jobChannelName, msg)

	if res.Val() < 1 {
		http.Error(w, "Action not processed", http.StatusServiceUnavailable)
		log.WithFields(log.Fields{
			"job": r.Header.Get(hookJobHeaderName),
		}).Info("No agent for job")
		return
	}

	w.Write([]byte(jobID))

	log.WithFields(log.Fields{
		"job": r.Header.Get(hookJobHeaderName),
		"id":  jobID,
	}).Info("Job accepted")

}

func testToken(r *http.Request) *MiddlewareError {
	if !hasValidToken(r) {
		return &MiddlewareError{
			http.StatusForbidden,
			"Forbidden",
			log.Fields{
				"job": r.Header.Get(hookJobHeaderName),
			},
			"Invalid token",
		}
	}

	return nil
}

type jobResponse struct {
	Info  []string `json:"info"`
	Error []string `json:"error"`
}

func hasJob(jobID string) bool {
	if jobID == "" {
		return false
	}
	keys, err := redisClient.Keys(redisKeyPrefix + jobID).Result()

	return err == nil && len(keys) == 1
}

func handleGetJob(w http.ResponseWriter, r *http.Request) {
	pathSplit := strings.SplitAfter(r.URL.Path, "/job/")

	jobID := pathSplit[1]

	if !hasJob(jobID) {
		fmt.Println("no jobid")
		http.NotFound(w, r)
		return
	}

	infos := redisrangereader.NewRedisRangeReader(redisClient, redisKeyPrefix+jobID)
	io.Copy(w, infos)
}

// RequestHandler Handles requests to the root path
func RequestHandler(w http.ResponseWriter, r *http.Request) {
	if err := testToken(r); err != nil {
		log.WithFields(err.LogFields).Error(err.LogFields)
		http.Error(w, err.Text, err.Code)
		return
	}

	if r.Method == http.MethodGet {
		if strings.HasPrefix(r.URL.Path, "/job/") {
			handleGetJob(w, r)
			return
		}
	}

	if r.Method == http.MethodPost {
		handleNewJobRequest(w, r)
		return
	}

	http.NotFound(w, r)
}
